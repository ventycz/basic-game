﻿using System;
using System.Management;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading;

namespace BasicGame
{
    class ControllerManager
    {
        private object locker = new object();
        bool listening = false;

        public event GotDataDelegate GotData;
        public delegate void GotDataDelegate(SerialPort p, string data);

        List<SerialPort> listeningOn = new List<SerialPort>();
        List<SerialPort> connectedControllers = new List<SerialPort>();

        class SerialPortInfo
        {
            public SerialPortInfo(ManagementObject obj)
            {
                string portName = (string)obj["DeviceID"];
                string description = (string)obj["Caption"];
            }
        }

        public void GetSerialPorts()
        {
            //TODO: Test and use

            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_SerialPort");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    var nfo = new SerialPortInfo(queryObj);
                }
            }
            catch (ManagementException e)
            {
                Console.WriteLine("RIPPP: {0}", e.Message);
            }
        }

        public List<string> Ports()
        {
            string[] unsorted = SerialPort.GetPortNames();
            var sorted = (from string x in unsorted
                        		orderby int.Parse(x.Substring(3))
                        		select x);

            return sorted.ToList();
        }

        public void Listen(int baud = 57600)
        {
            if (listening) return;
            listening = true;

            new Thread(delegate ()
            {
                foreach (var item in listeningOn) { item.DataReceived -= S_DataReceived; item.Close(); }
                listeningOn.Clear();

                foreach (var port in Ports())
                {
                    try
                    {
                        var s = new SerialPort(port, baud);
                        s.DataReceived += S_DataReceived;
                        s.NewLine = "\r\n"; //Arduino Serial.Println();
                        s.Open();

                        listeningOn.Add(s);
                    }
                    catch (Exception) { Console.WriteLine("Cannot start listening on port {0}", port); }
                }
            }).Start();
        }

        private void S_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort p = (sender as SerialPort);

            string line = p.ReadLine().Trim();

            if (line == "PS2_CONTROLLER")
            {
                if (!connectedControllers.Contains(p))
                {
                    p.DataReceived -= S_DataReceived;

                    Console.WriteLine("Found a controller... @ {0}", p.PortName);
                    connectedControllers.Add(p);
                    p.DataReceived += delegate (object senderx, SerialDataReceivedEventArgs ex)
                    {
                        var ev = GotData;
                        if(ev != null) { ev.Invoke((SerialPort)senderx, ((SerialPort)senderx).ReadLine().Trim()); }
                    };
                }
            }
        }
    }
}
