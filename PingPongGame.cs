﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BasicGame
{
    class PingPongGame
    {
        public event EndGameDelegate EndGame;
        public delegate void EndGameDelegate(Players won, Players lost);

        Canvas gameBoard { get; set; }
        double BoardWidth { get { return (gameBoard.ActualWidth); } }
        double BoardHeight { get { return (gameBoard.ActualHeight); } }

        Dispatcher GameThread { get { return Application.Current.Dispatcher; } }

        public PingPongGame() { }

        private Border player_bottom;
        private Border player_top;
        private Ellipse ball = null;

        class CanvasPosition
        {
            private Window holder;
            private Canvas c;
            private FrameworkElement el;

            private Point GetPosition()
            {
                return el.TranslatePoint(new Point(0, 0), c);
            }

            private Point GetAbsolutePosition()
            {
                return el.TranslatePoint(new Point(0, 0), holder);
            }

            private static Window GetHolder(FrameworkElement elem)
            {
                FrameworkElement dep = elem;
                do { dep = (FrameworkElement)dep.Parent; } while (!(dep is Window) && dep != null);

                if (dep != null) { return dep as Window; }
                return null;
            }

            public static CanvasPosition Get(FrameworkElement el)
            {
                var parent = el.Parent as FrameworkElement;

                if (parent is Canvas) { return new CanvasPosition() { el = el, c = parent as Canvas, holder = GetHolder(el) }; }

                return null;
            }

            public double Top
            {
                get { return GetPosition().Y; }
                set { Canvas.SetTop(el, value); }
            }
            public double Left
            {
                get { return GetPosition().X; }
                set { Canvas.SetLeft(el, value); }
            }
            public double Bottom
            {
                get { return c.ActualHeight - (GetPosition().Y + el.ActualHeight); }
                set { Canvas.SetTop(el, GetPosition().Y + (Bottom - value)); }
            }
            public double Right
            {
                get { return c.ActualWidth - (GetPosition().X + el.ActualWidth); }
                set { Canvas.SetLeft(el, GetPosition().X + (Right - value)); }
            }
        }

        public enum Players
        {
            Top,
            Bottom
        }

        public enum Directions
        {
            Undefined,

            Left,
            Right,

            NW,
            NE,
            SW,
            SE
        }

        public void PrepareGame(Canvas gameBoard)
        {
            this.gameBoard = gameBoard;

            //Clear out the game board
            gameBoard.Children.Clear();

            //Do stuff
            SpawnPlayers();
            SpawnBall();
        }

        public void Begin()
        {
            if (ballTimer != null)
                return;

            Console.WriteLine("Game just started!!");

            ballTimer = new System.Timers.Timer(10);
            ballTimer.Elapsed += Timer_Elapsed;
            ballTimer.Start();
        }

        public void Stop()
        {
            if (ballTimer == null)
                return;

            Console.WriteLine("Game just ended");

            ballTimer.Stop();
            ballTimer.Dispose();
            ballTimer = null;
        }

        private Directions ball_lastDirection = Directions.Undefined;

        System.Timers.Timer ballTimer;
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //TODO: Choose random direction
            if (ball_lastDirection == Directions.Undefined) { ball_lastDirection = Directions.SE; }

            GameThread.Invoke(delegate ()
            {
                int by = 5;

                MoveBall(ball_lastDirection, by, by);

                var ballPos = CanvasPosition.Get(ball);
                var p_bottom = CanvasPosition.Get(player_bottom);
                var p_top = CanvasPosition.Get(player_top);

                bool hit_bottom = ballPos.Bottom < p_bottom.Bottom + player_bottom.ActualHeight;
                bool hit_top = ballPos.Top < p_top.Top + player_top.ActualHeight;
                bool hit_left = ballPos.Left < 0;
                bool hit_right = ballPos.Right < 0;

                //TODO: Add possibility to register a hit even on small portion of the player platform

                if (hit_bottom)
                {
                    var pLeft = p_bottom.Left;
                    var pRight = p_bottom.Right;

                    var bLeft = ballPos.Left;
                    var bRight = ballPos.Right;

                    bool hit = bLeft >= pLeft && bRight >= pRight;

                    if (!hit)
                    {
                        var ev = EndGame;
                        if (ev != null) { ev.Invoke(Players.Top, Players.Bottom); }
                    }

                    if (hit_left)
                    {
                        ball_lastDirection = Directions.NE;
                    }
                    else if (hit_right)
                    {
                        ball_lastDirection = Directions.NW;
                    }
                    else
                    {
                        if (ball_lastDirection == Directions.SE)
                        {
                            ball_lastDirection = Directions.NE;
                        }
                        else if (ball_lastDirection == Directions.SW)
                        {
                            ball_lastDirection = Directions.NW;
                        }
                    }
                }
                else if (hit_top)
                {
                    var pLeft = p_top.Left;
                    var pRight = p_top.Right;

                    var bLeft = ballPos.Left;
                    var bRight = ballPos.Right;

                    bool hit = bLeft >= pLeft && bRight >= pRight;

                    if (!hit)
                    {
                        var ev = EndGame;
                        if (ev != null) { ev.Invoke(Players.Bottom, Players.Top); }
                    }

                    if (hit_left)
                    {
                        ball_lastDirection = Directions.SE;
                    }
                    else if (hit_right)
                    {
                        ball_lastDirection = Directions.SW;
                    }
                    else
                    {
                        if (ball_lastDirection == Directions.NE)
                        {
                            ball_lastDirection = Directions.SE;
                        }
                        else if (ball_lastDirection == Directions.NW)
                        {
                            ball_lastDirection = Directions.SW;
                        }
                    }
                }
                else if (hit_left)
                {
                    if (ball_lastDirection == Directions.NW)
                    {
                        ball_lastDirection = Directions.NE;
                    }
                    else if (ball_lastDirection == Directions.SW)
                    {
                        ball_lastDirection = Directions.SE;
                    }
                }
                else if (hit_right)
                {
                    if (ball_lastDirection == Directions.NE)
                    {
                        ball_lastDirection = Directions.NW;
                    }
                    else if (ball_lastDirection == Directions.SE)
                    {
                        ball_lastDirection = Directions.SW;
                    }
                }

                MoveBall(ball_lastDirection, by, by);
            });
        }

        private void SpawnPlayers()
        {
            //Create players
            player_bottom = new Border();
            player_top = new Border();

            //Add them to the board
            gameBoard.Children.Add(player_bottom);
            gameBoard.Children.Add(player_top);

            //Set them up
            player_bottom.Background = Brushes.Gray;
            player_top.Background = Brushes.Black;

            player_bottom.Width = player_top.Width = 200;
            player_bottom.Height = player_top.Height = 15;

            player_bottom.CornerRadius = player_top.CornerRadius = new CornerRadius(10);

            //Move to positions
            Canvas.SetBottom(player_bottom, 5);
            Canvas.SetTop(player_top, 5);

            Canvas.SetLeft(player_bottom, (BoardWidth / 2) - (player_bottom.Width / 2));
            Canvas.SetLeft(player_top, (BoardWidth / 2) - (player_top.Width / 2));
        }

        private void SpawnBall()
        {
            ball = new Ellipse();
            gameBoard.Children.Add(ball);

            ball.Width = ball.Height = 30;

            ball.Fill = Brushes.Red;

            Canvas.SetLeft(ball, (BoardWidth / 2) - (ball.Width / 2));
            Canvas.SetTop(ball, (BoardHeight / 2) - (ball.Height / 2));
        }

        public void MovePlayer(Players which, Directions where, double by)
        {
            GameThread.Invoke(delegate ()
            {
                Border player = null;
                by = Math.Abs(by);

                //Gets player
                if (which == Players.Bottom) { player = player_bottom; }
                else if (which == Players.Top) { player = player_top; }
                else { return; }

                //Gets players position
                CanvasPosition pos = CanvasPosition.Get(player);

                //Moves the player
                if (where == Directions.Left && pos.Left - by >= 0) { pos.Left -= by; }
                else if (where == Directions.Right && pos.Right - by >= 0) { pos.Right -= by; }
            });
        }

        public void MoveBall(Directions dir, double byX, double byY)
        {
            GameThread.Invoke(delegate ()
            {
                var pos = CanvasPosition.Get(ball);

                if (dir == Directions.SW)
                {
                    pos.Right += byX;
                    pos.Top += byY;
                }
                else if (dir == Directions.SE)
                {
                    pos.Left += byX;
                    pos.Top += byY;
                }
                else if (dir == Directions.NW)
                {
                    pos.Right += byX;
                    pos.Bottom += byY;
                }
                else if (dir == Directions.NE)
                {
                    pos.Left += byX;
                    pos.Bottom += byY;
                }
            });
        }
    }
}
