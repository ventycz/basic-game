﻿using System;
using System.IO.Ports;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Media;

namespace BasicGame
{
    public partial class MainWindow : Window
    {
        private PingPongGame game;
        private ControllerManager controllers;

        public MainWindow()
        {
            InitializeComponent();

            game = new PingPongGame();
            game.EndGame += Game_EndGame;
            controllers = new ControllerManager();

            Loaded += MainWindow_Loaded;
            KeyDown += MainWindow_KeyDown;

            //controllers.GetSerialPorts();
            controllers.GotData += Controllers_GotData;
            controllers.Listen(57600);

            Timer clrTimer = new Timer(100);
            clrTimer.Elapsed += ClrTimer_Elapsed;
            clrTimer.Start();

            Timer tmr = new Timer(20);
            tmr.Elapsed += Tmr_Elapsed;
            tmr.Start();
        }

        private void ClrTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var randomizer = new Random();
            //SolidColorBrush brush = new SolidColorBrush(Color.FromRgb((byte)randomizer.Next(0, 255), (byte)randomizer.Next(0, 255), (byte)randomizer.Next(0, 255)));

            ChangeColor(randomizer.Next(0, 255), randomizer.Next(0, 255), randomizer.Next(0, 255));
        }

        private void ChangeColor(int red, int blue, int green)
        {
            if (Dispatcher.CheckAccess())
            {
                Background = new SolidColorBrush(Color.FromRgb((byte)red, (byte)blue, (byte)green));
            }
            else
            {
                Dispatcher.Invoke(delegate () { ChangeColor(red, green, blue); });
            }
        }

        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            double by = 20;

            if (top_pad_left)
            {
                game.MovePlayer(PingPongGame.Players.Top, PingPongGame.Directions.Left, by);
            }
            else if (top_pad_right)
            {
                game.MovePlayer(PingPongGame.Players.Top, PingPongGame.Directions.Right, by);
            }

            if (bottom_pad_left)
            {
                game.MovePlayer(PingPongGame.Players.Bottom, PingPongGame.Directions.Left, by);
            }
            else if (bottom_pad_right)
            {
                game.MovePlayer(PingPongGame.Players.Bottom, PingPongGame.Directions.Right, by);
            }
        }

        bool top_pad_left = false;
        bool top_pad_right = false;
        bool bottom_pad_left = false;
        bool bottom_pad_right = false;

        Regex regex_leftRight = new Regex(@"PS2_CONTROLLER\sLEFT=(TRUE|FALSE)\sRIGHT=(TRUE|FALSE)");

        SerialPort player_bottom;
        SerialPort player_top;
        private void Controllers_GotData(SerialPort p, string data)
        {
            if (data != "PS2_CONTROLLER")
            {
                if (data == "PS2_CONTROLLER_ERROR")
                {
                    Console.WriteLine("ERROR CONTROLLER");
                }
                else
                {
                    Console.WriteLine(data);

                    var m = regex_leftRight.Match(data);
                    if (m.Success)
                    {
                        if (p == player_top)
                        {
                            top_pad_left = m.Groups[1].Value == "TRUE";
                            top_pad_right = m.Groups[2].Value == "TRUE";
                        }
                        else if (p == player_bottom)
                        {
                            bottom_pad_left = m.Groups[1].Value == "TRUE";
                            bottom_pad_right = m.Groups[2].Value == "TRUE";
                        }
                    }
                }
            }
            else
            {
                if (p != player_bottom && p != player_top)
                {
                    if (player_bottom == null)
                    { player_bottom = p; }
                    else
                    { player_top = p; }
                }
            }
        }

        private void Game_EndGame(PingPongGame.Players won, PingPongGame.Players lost)
        {
            Console.WriteLine("Win: {0}, Lost: {1}", won, lost);
            game.Stop();
        }

        private void MainWindow_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            double by = 20;

            if (e.Key == System.Windows.Input.Key.Left)
            {
                game.MovePlayer(PingPongGame.Players.Bottom, PingPongGame.Directions.Left, by);
            }
            else if (e.Key == System.Windows.Input.Key.Right)
            {
                game.MovePlayer(PingPongGame.Players.Bottom, PingPongGame.Directions.Right, by);
            }
            else if (e.Key == System.Windows.Input.Key.A)
            {
                game.MovePlayer(PingPongGame.Players.Top, PingPongGame.Directions.Left, by);
            }
            else if (e.Key == System.Windows.Input.Key.D)
            {
                game.MovePlayer(PingPongGame.Players.Top, PingPongGame.Directions.Right, by);
            }
            else if (e.Key == System.Windows.Input.Key.Enter)
            {
                game.Begin();
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            game.PrepareGame(GameZone);
        }
    }
}
