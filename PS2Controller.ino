#include <Arduino.h>
#include <Arduino-PS2X-master\PS2X_lib\PS2X_lib.h>

#define PS2_DAT        8
#define PS2_CMD        11
#define PS2_SEL        10
#define PS2_CLK        12

/******************************************************************
	* select modes of PS2 controller:
	*   - pressures = analog reading of push-butttons
	*   - rumble    = motor rumbling
	* uncomment 1 of the lines for each mode selection
	******************************************************************/
#define pressures   true
	//#define pressures   false
#define rumble      true
//#define rumble      false

PS2X ps2x; // create PS2 Controller Class

//right now, the library does NOT support hot pluggable controllers, meaning 
//you must always either restart your Arduino after you connect the controller, 
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;

void printInfo(int er, byte tp)
{
    if (er == 0) {
        Serial.print("> Found Controller");
    }
    else if (er == 1)
        Serial.println("No controller found !");
    else if (er == 2)
        Serial.println("Controller found but not accepting commands !");
    else if (er == 3)
        Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

    switch (tp) {
    case 0:
        Serial.println("Unknown Controller type found ");
        break;
    case 1:
        Serial.println("DualShock Controller found ");
        break;
    default:
        break;
    }
}

void PrepareController()
{
    //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
    error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    type = ps2x.readType();

    printInfo(error, type);
}

void PrintTestControllerState()
{
    if (ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed
        Serial.println("Start is being held");
    if (ps2x.Button(PSB_SELECT))
        Serial.println("Select is being held");

    if (ps2x.Button(PSB_PAD_UP)) {      //will be TRUE as long as button is pressed
        Serial.print("Up held this hard: ");
        Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
    }
    if (ps2x.Button(PSB_PAD_RIGHT)) {
        Serial.print("Right held this hard: ");
        Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
    }
    if (ps2x.Button(PSB_PAD_LEFT)) {
        Serial.print("LEFT held this hard: ");
        Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
    }
    if (ps2x.Button(PSB_PAD_DOWN)) {
        Serial.print("DOWN held this hard: ");
        Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
    }

    vibrate = ps2x.Analog(PSAB_CROSS);

    if (ps2x.NewButtonState()) {
        if (ps2x.Button(PSB_L3))
            Serial.println("L3 pressed");
        if (ps2x.Button(PSB_R3))
            Serial.println("R3 pressed");
        if (ps2x.Button(PSB_L2))
            Serial.println("L2 pressed");
        if (ps2x.Button(PSB_R2))
            Serial.println("R2 pressed");
        if (ps2x.Button(PSB_TRIANGLE))
            Serial.println("Triangle pressed");
    }

    if (ps2x.ButtonPressed(PSB_CIRCLE))               //will be TRUE if button was JUST pressed
        Serial.println("Circle just pressed");
    if (ps2x.NewButtonState(PSB_CROSS))               //will be TRUE if button was JUST pressed OR released
        Serial.println("X just changed");
    if (ps2x.ButtonReleased(PSB_SQUARE))              //will be TRUE if button was JUST released
        Serial.println("Square just released");

    if (ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) { //print stick values if either is TRUE
        Serial.print("Stick Values:");
        Serial.print(ps2x.Analog(PSS_LY), DEC); //Left stick, Y axis. Other options: LX, RY, RX  
        Serial.print(",");
        Serial.print(ps2x.Analog(PSS_LX), DEC);
        Serial.print(",");
        Serial.print(ps2x.Analog(PSS_RY), DEC);
        Serial.print(",");
        Serial.println(ps2x.Analog(PSS_RX), DEC);
    }
}

bool go_left = false;
bool go_right = false;
void PrintControllerState()
{
    bool X_Changed = ps2x.NewButtonState(PSB_CROSS);
    bool Rect_Changed = ps2x.NewButtonState(PSB_SQUARE);
    bool Triangle_Changed = ps2x.NewButtonState(PSB_GREEN);
    bool O_Changed = ps2x.NewButtonState(PSB_CIRCLE);

    bool PAD_DOWN = ps2x.Button(PSB_PAD_DOWN);
    bool PAD_UP = ps2x.Button(PSB_PAD_UP);
    bool PAD_LEFT = ps2x.Button(PSB_PAD_LEFT);
    bool PAD_RIGHT = ps2x.Button(PSB_PAD_RIGHT);

    if (X_Changed)
    {
        Serial.println("X: " + (String)(ps2x.ButtonPressed(PSB_CROSS) ? "Pressed" : "Released"));
    }

    if (Rect_Changed)
    {
        Serial.println("Rect: " + (String)(ps2x.ButtonPressed(PSB_SQUARE) ? "Pressed" : "Released"));
    }

    if (Triangle_Changed)
    {
        Serial.println("Triangle: " + (String)(ps2x.ButtonPressed(PSB_GREEN) ? "Pressed" : "Released"));
    }

    if (O_Changed)
    {
        Serial.println("Circle: " + (String)(ps2x.ButtonPressed(PSB_CIRCLE) ? "Pressed" : "Released"));
    }

    /*bool pad_left_changed = ps2x.NewButtonState(PSB_PAD_LEFT);
    bool pad_right_changed = ps2x.NewButtonState(PSB_PAD_RIGHT);

    if (pad_left_changed || pad_right_changed)
    {
        Serial.println("PS2_CONTROLLER LEFT=" + (String)(PAD_LEFT ? "TRUE" : "FALSE") + " RIGHT=" + (String)(PAD_RIGHT ? "TRUE" : "FALSE"));
    }*/

    /*if (PAD_DOWN || PAD_UP || PAD_LEFT || PAD_RIGHT)
    {
        Serial.println("UP: " + (String)(PAD_UP ? "true" : "false") + " DOWN: " + (String)(PAD_DOWN ? "true" : "false") + " LEFT: " + (String)(PAD_LEFT ? "true" : "false") + " RIGHT: " + (String)(PAD_RIGHT ? "true" : "false"));
    }*/

    int y = (int)ps2x.Analog(PSS_RY);
    int x = (int)ps2x.Analog(PSS_RX);

    int min = 0;
    int max = 255;
    int center = max / 2;

    int right = max - x;
    int left = x;

    bool b_right = right < 100;
    bool b_left = left < 100;

    if (b_right != go_right || b_left != go_left)
    {
        go_right = b_right;
        go_left = b_left;

        Serial.println("PS2_CONTROLLER LEFT=" + (String)(go_left ? "TRUE" : "FALSE") + " RIGHT=" + (String)(go_right ? "TRUE" : "FALSE"));
    }

    vibrate = 0;
}

void ReadController()
{
    /* You must Read Gamepad to get new values and set vibration values
    			ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255)
    			if you don't enable the rumble, use ps2x.read_gamepad(); with no values
    			You should call this at least once a second
    	*/
    	//if (error == 1 || error1 == 1) //skip loop if no controller found
    	//    return;

    if (error == 1)
    {
        error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
        type = ps2x.readType();

        printInfo(error, type);
    }


    if (type != 1) //do not use anything but Dualshock Controller
        return;

    //read controller and set large motor to spin at 'vibrate' speed
    ps2x.read_gamepad(false, vibrate);

    PrintControllerState();
}

void setup() {
    delay(500);

    Serial.begin(57600);

    PrepareController();
}

unsigned long lastTime = 0;
void loop() {
    ReadController();

    if (millis() - lastTime >= 1000)
    {
        lastTime = millis();

        Serial.println(error == 1 ? "PS2_CONTROLLER_ERROR" : "PS2_CONTROLLER");
    }
}
